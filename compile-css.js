const sass = require('node-sass');
const fs = require('fs');

sass.render({
	file: __dirname + '/' + process.argv[2] + '/style.scss',
}, function(err, result){
	fs.writeFile(__dirname + '/' + process.argv[2] + '/style.css', result.css);
});

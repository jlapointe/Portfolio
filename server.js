const http = require('http');
const express = require('express');
const sass = require('node-sass');
const fs = require('fs');

var app = express();
var httpServer = http.Server(app);

app.use(express.static(__dirname));

app.get('/', (req,res) => {
  res.sendFile(__dirname + '/index.html');
});

app.get('/Brandly', (req,res) => {
  res.sendFile(__dirname + '/Brandly/index.html');
});

app.get('/Column', (req,res) => {
  res.sendFile(__dirname + '/Column/index.html');
});

app.get('/Take', (req,res) => {
  res.sendFile(__dirname + '/Take/index.html');
});

/*app.use(express.static(__dirname + '/Brandly'));
app.use(express.static(__dirname + '/Column'));
app.use(express.static(__dirname + '/Take'));*/

httpServer.listen(3000);
console.log("Serving on port 3000");
